const express = require('express')
const { Restaurant, Menu, Item } = require('studentbeans-restaurants')
const restaurantRoutes = require('./routes/restaurants')
const menuRoutes = require('./routes/menus')
const app = express()
const PORT = process.env.PORT || 3000

app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use('/restaurants', restaurantRoutes)
app.use('/restaurants/:restaurant_id/menus', menuRoutes)

app.listen(PORT, () => {
    Item.init()
    Menu.init()
    Restaurant.init()
    console.info(`server started on port ${PORT}`)
})
 