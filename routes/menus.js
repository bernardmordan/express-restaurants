const express = require('express')
const routes = express.Router({ mergeParams: true })
const { Restaurant, Menu } = require('studentbeans-restaurants')

routes.post('/', (req, res) => {
    const menu = new Menu(req.params.restaurant_id, req.body.title)
    const restaurant = Restaurant.all.find(r => String(r.id) === req.params.restaurant_id)
    restaurant.addMenu(menu)
    res.send(menu)
})

routes.put('/:id', (req, res) => {
    const restaurant = Restaurant.all.find(r => String(r.id) === req.params.restaurant_id)
    const menu = restaurant.menus.find(m => String(m.id) === req.params.id) 
    menu.update(req.body)
    res.send(menu)
})

module.exports = routes