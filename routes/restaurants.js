const express = require('express')
const router = express.Router()
const { Restaurant } = require('studentbeans-restaurants')

router.get('/', function (req, res) {
    res.send(Restaurant.all)
})

router.post('/', (req, res) => {
    const {name, imageURL} = req.body
    const restaurant = new Restaurant(name, imageURL)
    res.status(201).send(restaurant)
})

router.get('/:id', (req, res) => {
    const id = req.params.id
    const restaurant = Restaurant.all.find(restaurant => restaurant.id === Number(id))
    if (restaurant) {
        res.send(restaurant)
    } else {
        res.sendStatus(404)
    }
})

router.put('/:id', (req, res) => {
    const id = req.params.id
    const restaurant = Restaurant.all.find(restaurant => restaurant.id === Number(id))
    if (!restaurant) return res.sendStatus(404)
    restaurant.update(req.body)
    res.send(restaurant)
})

router.delete('/:id', (req, res) => {
    const id = req.params.id
    const restaurant = Restaurant.all.find(restaurant => restaurant.id === Number(id))
    if (!restaurant) return res.sendStatus(404)
    restaurant.delete()
    res.sendStatus(204)
})

module.exports = router