# Express Restaurants

This week is all about exposing our data model over a network.

1. clone this repo
1. `npm install`
1. `npm run dev` for a server that will reload upon changes

## Deploy to AWS

### AWS account

Create for yourself an AWS account (you'll have to add a credit/debit card).

### Provision EC2

Create an EC2 instance from the free tier. Just stick with the defaults. In the security section add a RULE for HTTP and select port 80. During this process you will generate a pair of keys you need to keep track of the .pem file as that is your only way to access your new server. Move the downloaded file somewhere safe that you can reference later. I place mine in `~/.ssh/` along side the keys I use to access Github etc. You will also have to restrict the permissions of the .pem file which you can do like this.

```sh
chmod 400 ./path/to/.pem
```

### Connect

Open a secure shell on your remote machine like this:

```sh
ssh -i ./path/to/you/key.pem ec2-user@x.x.x.x
```
replace the `x.x.x.x` with the ip address of your EC2 instance.

### Setup

On your remote machine you'll have to install a few tools to build the binary for SQLite3.

```sh
sudo yum groupinstall "Development Tools"
```

Then install nodejs via the node version manager

```sh
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
```
(you might need to exit the ssh session here type `exit` ENTER, and reconnect to reload your shell with the ssh command)
Now install node with `nvm install node`

### Upload

Exit your server. Then locally on your machine upload your files to your server. To upload our code base make a `copy.txt` file and list on each line the things you want copied to your server.

```sh
package.json
server.js
public
routes

```
Then upload those files with this command (you'll have to insert your .pem file path and ip address). Include blank line at end of file.

```sh
while read thingToCopy; do scp -i ./path/to/you/key.pem -r ./${thingToCopy} ec2-user@x.x.x.x:/home/ec2-user/.;done < copy.txt
```

### Install and start

1. ssh into your remote server
1. run `npm install`
1. get the path to node using `which node`
1. start the service like this

```sh
sudo PORT=80 ~/.nvm/versions/node/v17.7.1/bin/node server.js &
```
Notice the little `&` at the end that will run your service in the background. If you now visit your server's ip address in a browser you should see your hello world page, and you should be able to hit your endpoints and interact with your service.

If you need to stop the server, or you get a message about port 80 already being in use the try the following:

```sh
sudo lsof -i :80
```
That will return a PID number. With that PID number you need to run the command:
```sh
sudo kill 17631
```
Where 17631 is your PID number for your process.

[Watch the Video here](https://www.loom.com/share/9753f812183145b29b003bbc9e6b8e5c)
